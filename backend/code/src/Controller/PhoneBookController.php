<?php
/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 1/31/19
 * Time: 11:07 PM
 */

namespace App\Controller;


use App\Entity\Phone;
use App\Repository\PhoneRepository;
use App\Service\ValidationService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Exception;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PhoneBookController
 * @package App\Controller
 * @Route("/api/phones")
 */
class PhoneBookController extends AbstractController
{
    /**
     * @Route("/{id}", methods={"POST", "PUT"}, requirements={"id": "\d+"}, defaults={"id": 0})
     * @param int                $id
     * @param Request            $request
     * @param ValidatorInterface $validator
     * @param ValidationService  $countriesService
     * @param LoggerInterface    $logger
     *
     * @return JsonResponse
     * @throws InvalidArgumentException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function createUpdateAction(
        int $id,
        Request $request,
        ValidatorInterface $validator,
        ValidationService $countriesService,
        LoggerInterface $logger
    ): JsonResponse
    {
        try {
            $data = \GuzzleHttp\json_decode($request->getContent(), true);
        } catch (\InvalidArgumentException $e) {
            return $this->json([
                'status' => 'failure',
                'errors' => [
                    'Invalid request',
                ],
            ]);
        }

        /**
         * @var EntityManager $em
         */
        $em = $this->getDoctrine()->getManager();
        $firstName = trim(
            filter_var(
                $data['first_name'] ?? '',
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_NO_ENCODE_QUOTES
            )
        );
        $lastName = trim(
            filter_var(
                $data['last_name'] ?? '',
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_NO_ENCODE_QUOTES
            )
        );
        $phoneNumber = filter_var($data['phone_number'] ?? '', FILTER_SANITIZE_STRING);
        $countryCode = strtoupper(
            filter_var($data['country_code'] ?? '', FILTER_SANITIZE_STRING)
        );
        $timezone = trim(
            filter_var($data['timezone'] ?? '',
                FILTER_SANITIZE_STRING,
                FILTER_FLAG_NO_ENCODE_QUOTES
            )
        );

        $errors = [];

        $countryCodeValid = true;
        $timezoneValid = true;
        try {
            if ($countryCode) {
                $countryCodeValid = $countriesService->validateCountryCode($countryCode);

            }

        } catch (Exception $e) {
            $logger->critical($e);
            $errors[] = 'Unable to validate country code';
        }

        try {
            if ($timezone) {
                $timezoneValid = $countriesService->validateTimezone($timezone);
            }

        } catch (Exception $e) {
            $logger->critical($e);
            $errors[] = 'Unable to validate timezone';
        }

        if (count($errors)) {
            return $this->json([
                'status' => 'failure',
                'errors' => $errors,
            ], Response::HTTP_SERVICE_UNAVAILABLE);
        }


        if (!$countryCodeValid) {
            $errors[] = 'Invalid country code';
        }

        if (!$timezoneValid) {
            $errors[] = 'Invalid timezone';
        }

        if ($request->getMethod() === Request::METHOD_POST) {
            $phone = new Phone();
        } else {
            $phone = $em->find(Phone::class, $id);

            if (!$phone) {
                return $this->json([
                    'status' => 'failure',
                    'errors' => [
                        'Phone record not found',
                    ],
                ], Response::HTTP_NOT_FOUND);
            }
        }


        $phone
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setPhoneNumber($phoneNumber)
            ->setCountryCode($countryCode)
            ->setTimezone($timezone);

        /**
         * @var ConstraintViolationList $validationErrors
         */
        $validationErrors = $validator->validate($phone);

        foreach ($validationErrors as $validationError) {
            $errors[] = $validationError->getMessage();
        }

        if (count($errors)) {
            return $this->json([
                'status' => 'failure',
                'errors' => $errors,
            ], Response::HTTP_BAD_REQUEST);
        }


        $em->persist($phone);
        $em->flush();


        return $this->json([
            'status' => 'success',
            'id'     => $phone->getId(),
        ]);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     * @Route("/{id}", requirements={"id": "\d+"}, methods={"DELETE"})
     */
    public function deleteAction(int $id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $phone = $em->find(Phone::class, $id);

        if (!$phone) {
            return $this->json([
                'status' => 'failure',
                'errors' => [
                    'Phone not found',
                ],
            ], Response::HTTP_NOT_FOUND);
        }

        $em->remove($phone);
        $em->flush();

        return $this->json([
            'status' => 'success',
        ]);
    }

    /**
     * @Route("/{id}", requirements={"id": "\d+"}, methods={"GET"})
     * @param int $id
     *
     * @return JsonResponse
     */
    public function readAction(int $id): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        /**
         * @var Phone $phone
         */
        $phone = $em->find(Phone::class, $id);

        if (!$phone) {
            return $this->json([
                'status' => 'failure',
                'errors' => [
                    'Phone not found',
                ],
            ], Response::HTTP_NOT_FOUND);
        }

        return $this->json([
            'status' => 'success',
            'data'   => $phone->jsonSerialize(),
        ]);
    }

    /**
     * @Route(
     *     "/list/{page}/{itemsPerPage}",
     *     methods={"GET"},
     *     requirements={"page": "\d+", "itemsPerPage": "\d+"},
     *     defaults={"page": 1, "itemsPerPage": 10}
     * )
     * @param int     $page
     * @param int     $itemsPerPage
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ORMException
     */
    public function listAction(int $page, int $itemsPerPage, Request $request): JsonResponse
    {
        $query = filter_var(
            $request->get('query', ''),
            FILTER_SANITIZE_STRING,
            FILTER_FLAG_NO_ENCODE_QUOTES
        );
        /**
         * @var EntityManager $em
         */
        $em = $this->getDoctrine()->getManager();
        /**
         * @var PhoneRepository $repo
         */
        $repo = $em->getRepository(Phone::class);
        /**
         * @var Phone[] $phones
         */
        [$phones, $totalCount] = $repo->getList($page, $itemsPerPage, $query);

        $data = [];

        foreach ($phones as $phone) {
            $data[] = $phone->jsonSerialize();
        }

        return $this->json([
            'status'     => 'success',
            'totalCount' => $totalCount,
            'data'       => $data,
        ]);
    }
}