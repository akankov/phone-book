<?php
/**
 * Created by IntelliJ IDEA.
 * User: alex
 * Date: 2/1/19
 * Time: 9:24 PM
 */

namespace App\Service;

use App\Exception\ValidationServiceException;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException;

class ValidationService
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var CacheService
     */
    private $cacheService;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(CacheService $cacheService, LoggerInterface $logger)
    {
        $this->client = new Client();
        $this->cacheService = $cacheService;
        $this->logger = $logger;
    }

    /**
     * @param string $countryCode
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function validateCountryCode(string $countryCode): bool
    {
        $codes = $this->cacheService->getItem('countries', function () {
            $response = $this->client->get('https://api.hostaway.com/countries');

            if ((int)$response->getStatusCode() !== 200) {
                $this->logger->critical('Unexpected status code: ' . $response->getStatusCode());
                throw new ValidationServiceException('Unable to verify country code');
            }

            $data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

            if (empty($data['result'])) {
                $this->logger->critical('Unexpected results: ' . json_encode($data));
                throw new ValidationServiceException('Unable to verify country code');
            }

            return array_keys($data['result']);
        });

        if (in_array($countryCode, $codes, true)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $timezone
     *
     * @return bool
     * @throws InvalidArgumentException
     */
    public function validateTimezone(string $timezone): bool
    {

        $timezones = $this->cacheService->getItem('timezones', function () {
            $response = $this->client->get('https://api.hostaway.com/timezones');

            if ((int)$response->getStatusCode() !== 200) {
                $this->logger->critical('Unexpected status code: ' . $response->getStatusCode());
                throw new ValidationServiceException('Unable to verify timezone');
            }

            $data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

            if (empty($data['result'])) {
                $this->logger->critical('Unexpected results: ' . json_encode($data));
                throw new ValidationServiceException('Unable to verify timezone');
            }

            return array_keys($data['result']);
        });

        if (in_array($timezone, $timezones, true)) {
            return true;
        }

        return false;
    }
}