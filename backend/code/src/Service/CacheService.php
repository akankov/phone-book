<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/2/19
 * Time: 12:02 PM
 */

namespace App\Service;

use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Cache\Simple\FilesystemCache;


class CacheService
{
    /**
     * @var FilesystemCache
     */
    private $adapter;

    public function __construct()
    {
        $this->adapter = new FilesystemCache();
    }

    /**
     * @param string        $key
     *
     * @param int           $ttl
     * @param callable|null $getData
     *
     * @return array|null
     * @throws InvalidArgumentException
     */
    public function getItem(string $key, callable $getData = null, int $ttl = 3600): ?array
    {
        if ($this->adapter->has($key)) {
            return $this->adapter->get($key);
        }

        if (is_callable($getData)) {
            $data = $getData();

            $this->adapter->set($key, $data, $ttl);

            return $data;
        }

        return null;


    }


}