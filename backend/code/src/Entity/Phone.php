<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhoneRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Phone implements JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="First name should not be blank")
     * @Assert\Length(max=255, maxMessage="First name is too long")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255, maxMessage="Last name is too long")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(message="Phone number should not be blank")
     * @Assert\Regex(pattern="/^[2-9]\d{2}-\d{3}-\d{4}$/", message="Phone number should have format like 800-555-5555")
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=2)
     * @Assert\Length(max=2, maxMessage="Country code is too long")
     */
    private $countryCode;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(max=50, maxMessage="Timezone is too long")
     */
    private $timezone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $insertedOn;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedOn;

    public function getInsertedOn(): ?\DateTimeInterface
    {
        return $this->insertedOn;
    }

    public function setInsertedOn(\DateTimeInterface $insertedOn): self
    {
        $this->insertedOn = $insertedOn;

        return $this;
    }

    public function getUpdatedOn(): ?\DateTimeInterface
    {
        return $this->updatedOn;
    }

    public function setUpdatedOn(\DateTimeInterface $updatedOn): self
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * @ORM\PreFlush()
     */
    public function preFlush(): self
    {
        if (!$this->insertedOn) {
            $this->setInsertedOn(new DateTime());
        }

        $this->setUpdatedOn(new DateTime());

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link  https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id'           => $this->getId(),
            'first_name'   => $this->getFirstName(),
            'last_name'    => $this->getLastName(),
            'phone_number' => $this->getPhoneNumber(),
            'country_code' => $this->getCountryCode(),
            'timezone'     => $this->getTimezone(),
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    public function setCountryCode(?string $countryCode): self
    {
        $this->countryCode = $countryCode;

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }
}
