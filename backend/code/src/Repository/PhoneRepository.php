<?php

namespace App\Repository;

use App\Entity\Phone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Phone|null find($id, $lockMode = null, $lockVersion = null)
 * @method Phone|null findOneBy(array $criteria, array $orderBy = null)
 * @method Phone[]    findAll()
 * @method Phone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhoneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Phone::class);
    }

    /**
     * @param $page
     * @param $itemsPerPage
     * @param $query
     *
     * @return array
     * @throws ORMException
     */
    public function getList($page, $itemsPerPage, $query): array
    {
        $qb = $this->createQueryBuilder('phone');

        if ($query) {
            $qb
                ->andWhere('phone.firstName like :query or phone.lastName like :query')
                ->setParameter('query', '%' . $query . '%');
        }

        $phones = $qb
            ->getQuery()
            ->setMaxResults($itemsPerPage)
            ->setFirstResult(($page - 1) * $itemsPerPage)
            ->getResult();

        $totalCount = (int)$qb
            ->select('count(phone)')
            ->getQuery()
            ->getSingleScalarResult();

        return [$phones, $totalCount];
    }
}
