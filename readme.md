# Hostaway. Senior PHP Backend Developer

to run this project on you local machine execute (docker and docker-compose required).
Nginx container will expose on port 8001. Make sure this port is available on your system 
or update port in ```docker/docker-compose-dev.yml``` file to any port you want.
```bash
cd project_dir
docker-compose -f docker/docker-compose-dev.yml up
docker exec -it phonebook_php sh
composer install
```

make sure database schema is up to date by running
```bash
docker exec -it phonebook_php sh
./bin/console doctrine:migration:migrate -n
```

## API docs
All REST API methods implemented in ```backend/code/src/Controller/PhoneBookController.php```

### Create phone book record
Curl:
```bash
curl --request POST \
  --url http://127.0.0.1:8001/api/phones \
  --header 'content-type: application/json' \
  --data '{
	"first_name": "Alex",
	"last_name": "Kankov",
	"timezone": "Asia/Barnaul",
	"country_code": "ru",
	"phone_number": "800-555-5555"
}'
```

HTTPie
```bash
echo '{
	"first_name": "Alex",
	"last_name": "Kankov",
	"timezone": "Asia/Barnaul",
	"country_code": "ru",
	"phone_number": "800-555-5555"
}' |  \
  http POST http://127.0.0.1:8001/api/phones \
  content-type:application/json
```

PHP
```php
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "8001",
  CURLOPT_URL => "http://127.0.0.1:8001/api/phones",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n\t\"first_name\": \"Alex\",\n\t\"last_name\": \"Kankov\",\n\t\"timezone\": \"Asia/Barnaul\",\n\t\"country_code\": \"ru\",\n\t\"phone_number\": \"800-555-5555\"\n}",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
```

### Update phone book record
Curl
```bash
curl --request PUT \
  --url http://127.0.0.1:8001/api/phones/1 \
  --header 'content-type: application/json' \
  --data '{
	"first_name": "Alex",
	"last_name": "Kankov",
	"timezone": "Asia/Barnaul",
	"country_code": "ru",
	"phone_number": "800-555-5555"
}'
```
HTTPie
```bash
echo '{
	"first_name": "Alex",
	"last_name": "Kankov",
	"timezone": "Asia/Barnaul",
	"country_code": "ru",
	"phone_number": "800-555-5555"
}' |  \
  http PUT http://127.0.0.1:8001/api/phones/1 \
  content-type:application/json
```

PHP
```php
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "8001",
  CURLOPT_URL => "http://127.0.0.1:8001/api/phones/1",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "PUT",
  CURLOPT_POSTFIELDS => "{\n\t\"first_name\": \"Alex\",\n\t\"last_name\": \"Kankov\",\n\t\"timezone\": \"Asia/Barnaul\",\n\t\"country_code\": \"ru\",\n\t\"phone_number\": \"800-555-5555\"\n}",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
```

### Delete record
Curl
```bash
curl --request DELETE \
  --url http://127.0.0.1:8001/api/phones/1 \
  --header 'content-type: application/json'
```

HTTPie
```bash
http DELETE http://127.0.0.1:8001/api/phones/1 \
  content-type:application/json
```
PHP
```php
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "8001",
  CURLOPT_URL => "http://127.0.0.1:8001/api/phones/1",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "DELETE",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
```

### Read phone book record
Curl
```bash
curl --request GET \
  --url http://127.0.0.1:8001/api/phones/3
```

HTTPie
```bash
http GET http://127.0.0.1:8001/api/phones/3 \
  content-type:application/json
```

PHP
```php
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "8001",
  CURLOPT_URL => "http://127.0.0.1:8001/api/phones/3",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
```

### Get list of records
Curl
```bash
curl --request GET \
  --url 'http://127.0.0.1:8001/api/phones/list/1/10?query=query' \
  --header 'content-type: application/json'
```
HTTPie
```bash
http GET 'http://127.0.0.1:8001/api/phones/list/1/10?query=query' \
  content-type:application/json
```

PHP
```php
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_PORT => "8001",
  CURLOPT_URL => "http://127.0.0.1:8001/api/phones/list/1/10?query=query",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
```